﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int amountGiven = 100;
            int itemCost = 45;

            int wisselgeld = amountGiven - itemCost;

            var Euro1 = "";
            var Cent50 = "";
            var Cent20 = "";
            var Cent10 = "";
            var Cent5 = "";
            var Cent2 = "";
            var Cent1 = "";

            Euro1 += wisselgeld / 100;
            wisselgeld = wisselgeld % 100;
            Cent50 += wisselgeld / 50;
            wisselgeld = wisselgeld % 50;
            Cent20 += wisselgeld / 20;
            wisselgeld = wisselgeld % 20;
            Cent10 += wisselgeld / 10;
            wisselgeld = wisselgeld % 10;
            Cent5 += wisselgeld / 5;
            wisselgeld = wisselgeld % 5;
            Cent2 += wisselgeld / 2;
            wisselgeld = wisselgeld % 2;
            Cent1 += wisselgeld / 1;
            wisselgeld = wisselgeld % 1;

            Console.WriteLine("Number of 1 euro coins returned is " + Euro1);
            Console.WriteLine("Number of 50 cent coins returned is " + Cent50);
            Console.WriteLine("Number of 20 cent coins returned is " + Cent20);
            Console.WriteLine("Number of 10 cent coins returned is " + Cent10);
            Console.WriteLine("Number of 5 cent coins returned is " + Cent5);
            Console.WriteLine("Number of 2 cent coins returned is " + Cent2);
            Console.WriteLine("Number of 1 cent coins returned is " + Cent1);
        }
    }
}
